#include "external.h"
#include <klee/klee.h>

int main(int argc, char **argv)
{
    
    int x = 10;
    int y = 15;
    
    klee_make_symbolic(&x, sizeof(x), "x");
    klee_make_symbolic(&y, sizeof(y), "y");

    if (func1(x, y) > 10)
    {
        return 11;
    }
    if (func2(x, y) < 10)
    {
        return 9;
    }
    return 0;

}