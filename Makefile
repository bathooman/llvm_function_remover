
KLEE_BUILD_PATH=/home/hooman/Repos/klee-augmented/build/


LDFLAGS = -L /home/hooman/Repos/symbolicexecution/kleener/build/lib -L . -lext -lkleeRuntest
SYMFLAGS = -Xclang -disable-llvm-passes -D__NO_STRING_INLINES  -D_FORTIFY_SOURCE=0 -U__OPTIMIZE__
CFLAGS = -I . -Wall -g -DDEBUG -O0 

CC = wllvm

apps: main

main: main.o libext.a
	$(CC) main.o -o main $(LDFLAGS)

main.o: main.c 
	$(CC) $(CFLAGS) $(SYMFLAGS) -c main.c -o main.o

libext.a: external.o
	ar -cvq libext.a external.o

external.o: external.c
	$(CC) $(CFLAGS) $(SYMFLAGS) -c external.c -o external.o

clean:
	rm -f main.o main
	rm -f main.bc
	rm -f libext.a
	rm -f external.o
	rm -f .*.bc